﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets._Scripts.Variables
{
    class GlobalVariables
    {
        public const String KNIGHT_RUN = "Knight_run";
        public const String KNIGHT_JUMP = "Knight_jump";
        public const String KNIGHT_DIE = "Knight_die";
        public const String KNIGHT_IDLE = "Knight_idle";

        public const String TAG_GRASS = "Grass";
        public const String TAG_COIN = "Coin";
        public const String TAG_PLAYER = "Player";

    }
}
