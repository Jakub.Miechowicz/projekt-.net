﻿using Assets._Scripts.Variables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KnightController : MonoBehaviour
{
    public bool isDead = false;

    public bool isGrounded;
    public LayerMask ground;
    public float checkRadius;
    public Transform feetPos;
    public bool facingRight = true;
    public Text pointsText;
    public Text winText;
    public Text deadText;

    private int points = 0;
    private float speed = 10f;
    private float jump = 10f;
    private Rigidbody2D rb2d;
    private int howManyJumps = 0;
    private AnimatorController animatorController;

    void Awake()
    {
        animatorController = GameObject.FindObjectOfType<AnimatorController>();
    }

    void Start()
    {
        points = 0;
        howManyJumps = 0;
        rb2d = GetComponent<Rigidbody2D>();
    }
    

    void Update()
    {
    }

    private void FixedUpdate()
    {
        if (isDead == false)
        {

            checkIfGrounded();
            FlipHorizontal();
            moveHorizontal();
            moveJump();
            idleAnim();
            flippingDirection();
            updatePointsText();



        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            Restart.RestartGame();
        }

    }
    
    void upDateDeadText()
    {
        if (points == 5)
        {
            pointsText.text = "";
            winText.text = "You are dead";
        }
    }


    void upDateWinText()
    {
        if(points == 5)
        {
            pointsText.text = "";
            winText.text = "You won!!!";
        }
    }

    void updatePointsText()
    {
        pointsText.text = "Points: " + points.ToString();
    }

    void onCollision2D(Collider2D other)
    {
        Debug.Log("KNIGHT MESSAGE");
        if (other.CompareTag("Coin"))
        {
            Debug.Log("KNIGHT MESSAGE");
            Destroy(other);
        }
    }

    void flippingDirection()
    {
        float h = Input.GetAxis("Horizontal");
        if (h > 0 && !facingRight)
            flip();
        else if (h < 0 && facingRight)
            flip();
    } 
  
    void flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    void FlipHorizontal()
    {
        float j = Input.GetAxisRaw("Horizontal");

    }

    void moveJump()
    {
        if (howManyJumps > 0 && Input.GetKeyDown(KeyCode.Space))
        {
            howManyJumps--;
            jumpAnim();
            rb2d.velocity = Vector2.up * jump;
        }
    }
    void moveHorizontal()
    {
        float moveHorizontal = Input.GetAxisRaw("Horizontal");
        runAnim();
        rb2d.velocity = new Vector2(moveHorizontal * speed, rb2d.velocity.y);
    }

    void checkIfGrounded()
    {
        isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, ground);
        if (isGrounded )
        {
            howManyJumps = 2;
        }
    }

    void jumpAnim()
    {
        animatorController.updateParameter(GlobalVariables.KNIGHT_JUMP);
    }

    void runAnim()
    {
        if (0 < Input.GetAxisRaw("Horizontal") || 0 > Input.GetAxisRaw("Horizontal"))
        {
            animatorController.updateParameter(GlobalVariables.KNIGHT_RUN);
        }
    }

    void idleAnim()
    {
        if ((Input.GetAxisRaw("Horizontal") == 0) && isDead == false)
        {
            animatorController.updateParameter(GlobalVariables.KNIGHT_IDLE);
        }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == GlobalVariables.TAG_GRASS || collision.gameObject.tag == GlobalVariables.TAG_COIN)
        {
            //fill with other objects

            
            if (collision.gameObject.tag == GlobalVariables.TAG_COIN)
            {
                Destroy(collision.gameObject);
                points++;

            }
        }
        else
        {
            isDead = true;
            animatorController.updateParameter(GlobalVariables.KNIGHT_DIE);
        }
    }
}
