﻿using Assets._Scripts.Variables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AnimatorController : MonoBehaviour
{
    private KnightController knightController;
    Animator anim;
    private string parameter = GlobalVariables.KNIGHT_IDLE;
    public float animateOnce = 1.0f;
    public bool facingRight = true;


    void Start()
    {
        anim = this.GetComponent<Animator>();
    }


    void Update()
    {


        switch (parameter)
        {
            case GlobalVariables.KNIGHT_RUN:
                anim.Play(GlobalVariables.KNIGHT_RUN);
                break;
            case GlobalVariables.KNIGHT_JUMP:
                anim.Play(GlobalVariables.KNIGHT_JUMP);
                break;
            case GlobalVariables.KNIGHT_DIE:
                anim.Play(GlobalVariables.KNIGHT_DIE);
                break;
            case GlobalVariables.KNIGHT_IDLE:
                anim.Play(GlobalVariables.KNIGHT_IDLE);
                break;
            default:
                anim.Play(GlobalVariables.KNIGHT_DIE);
                break;
        }
    }
    public void updateParameter(string update)
    {
        parameter = update;
    }
}
